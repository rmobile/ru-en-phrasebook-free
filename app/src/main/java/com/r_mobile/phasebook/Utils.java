package com.r_mobile.phasebook;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.view.View;

import com.r_mobile.phasebook.greenDao.Category;
import com.r_mobile.phasebook.greenDao.CategoryDao;
import com.r_mobile.phasebook.greenDao.DaoSession;
import com.r_mobile.phasebook.greenDao.Phrase;
import com.r_mobile.phasebook.greenDao.PhraseBookApp;
import com.r_mobile.phasebook.greenDao.Translate;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by AlanMalone on 02.03.2016.
 */

public class Utils {

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static View getParentTill(View target, int parentId) {
        View parent = (View) target.getParent();

        while(parent.getId() != parentId) {
            parent = (View) parent.getParent();
        }

        return parent;
    }

    public static void putPhraseInDatabase(InputStream textFile, DaoSession daoSession) {
        String[] temp = new String[4];
        String phrase;
        String transcription;
        String translate;
        String categoryName;
        long categoryID = 0;

        InputStreamReader fileReader = new InputStreamReader(textFile);
        Scanner scanner = new Scanner(fileReader);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            Log.d("phraseApp", "Изначальная строка: " + s);
            if (s.contains("/")) {
                temp = s.split("/");
                phrase = temp[0].trim();
                transcription = temp[1].trim();
                translate = temp[2].trim();

                Phrase newPhrase = new Phrase();
                newPhrase.setCategoryId(categoryID);
                newPhrase.setFavorite(0);
                newPhrase.setOwn(0);
                newPhrase.setPhrase(phrase);

                daoSession.getPhraseDao().insert(newPhrase);

                Translate newTranslate = new Translate();
                newTranslate.setPhraseId(newPhrase.getId());
                newTranslate.setLanguage("English");
                newTranslate.setContent(translate);
                newTranslate.setTranscription(transcription);

                daoSession.getTranslateDao().insert(newTranslate);

                Log.d("phraseApp", phrase + " " + transcription + " " + translate);
            } else {
                categoryName = s;
                Category category = new Category();
                category.setLabel(categoryName);
                daoSession.getCategoryDao().insert(category);
                categoryID++;
                Log.d("phraseApp", categoryName);
            }
        }
    }
}
